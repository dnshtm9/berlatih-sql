1.
CREATE DATABASE myshop;

2.
CREATE TABLE users (
	id int NOT NULL AUTO_INCREMENT,
	name VARCHAR (255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,
	PRIMARY KEY (id)
);
CREATE TABLE items (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    description varchar(255) NOT NULL,
    price int NOT NULL,
    stock int NOT NULL,
    category_id int,
    PRIMARY KEY (id),
    FOREIGN KEY (category_id) REFERENCES categories(id)
);
CREATE TABLE categories (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL
);

3.
INSERT INTO users (name, email, password)
VALUES ('John Doe', 'john@doe.com', 'john123'), ('Jane Doe', 'jane@doe.com', 'jenita123');
INSERT INTO categories (name)
VALUES ('gadget'), ('cloth'), ('men'), ('women'), ('branded');
INSERT INTO items (name, description, price, stock, category_id)
VALUES ('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1), ('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2), ('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);


4.
a.
SELECT id, name, email FROM users;
b.
SELECT * FROM items WHERE price > 1000000;
SELECT * FROM items WHERE name LIKE 'Uniklo%';
c.
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori
FROM items
INNER JOIN categories ON items.category_id = categories.category_id;

5.
UPDATE items
SET price = 2500000
WHERE name = 'Sumsang b50';